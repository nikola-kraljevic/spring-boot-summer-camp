package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Registration;
import org.springframework.data.repository.CrudRepository;

public interface RegistrationRepository extends CrudRepository<Registration, Long> {
}
