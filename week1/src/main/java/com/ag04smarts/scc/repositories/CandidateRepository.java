package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Candidate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/*
* RAW PASTE DATA FOR H2

SELECT CANDIDATE.FIRSTNAME, CANDIDATE.LASTNAME
FROM REGISTRATION INNER JOIN CANDIDATE ON  REGISTRATION.CANDIDATE_ID = CANDIDATE.ID
WHERE DATE > '2019-07-20 00:01:00' AND AGE > 21
*
*
SELECT CANDIDATE.FIRSTNAME, CANDIDATE.LASTNAME
FROM REGISTRATION_COURSE
    INNER JOIN REGISTRATION
        ON REGISTRATION.ID = REGISTRATION_COURSE.REGISTRATIONS_ID
    LEFT JOIN CANDIDATE
        ON CANDIDATE.ID = REGISTRATION.CANDIDATE_ID
    LEFT JOIN COURSE
        ON REGISTRATION_COURSE.COURSES_ID = COURSE.ID
WHERE COURSE.TYPE = 'BASIC'
AND CANDIDATE.GENDER = 'FEMALE'

* */

@Repository
public interface CandidateRepository extends CrudRepository<Candidate, Long> {

    @Query(value = "SELECT *\n" +
            "FROM REGISTRATION_COURSE\n" +
            "    INNER JOIN REGISTRATION\n" +
            "        ON REGISTRATION.ID = REGISTRATION_COURSE.REGISTRATIONS_ID\n" +
            "    LEFT JOIN CANDIDATE\n" +
            "        ON CANDIDATE.ID = REGISTRATION.CANDIDATE_ID\n" +
            "    LEFT JOIN COURSE\n" +
            "        ON REGISTRATION_COURSE.COURSES_ID = COURSE.ID\n" +
            "WHERE COURSE.TYPE = 'BASIC'\n" +
            "AND CANDIDATE.GENDER = 'FEMALE'", nativeQuery = true)
    Collection<Candidate>  getFemalesWithBasicCourses();

    @Query(value = "SELECT *\n" +
            "FROM REGISTRATION INNER JOIN CANDIDATE ON REGISTRATION.CANDIDATE_ID = CANDIDATE.ID\n" +
            "WHERE DATE > '2019-07-20 00:01:00' AND AGE > 21;\n", nativeQuery = true)
    Collection<Candidate> getCandidatesRegistrationAge();
}
