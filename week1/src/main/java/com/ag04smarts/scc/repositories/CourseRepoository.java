package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepoository extends CrudRepository<Course, Long> {
}
