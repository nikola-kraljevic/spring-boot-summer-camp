package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.exceptions.NotFoundException;
import com.ag04smarts.scc.services.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/api")
public class CandidateController {

    private final CandidateService candidateService;

    @Autowired
    public CandidateController(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @GetMapping("/candidates")
    public String getCandidates(Model model){
        model.addAttribute("candidates", candidateService.getCandidates());
        return "candidates";
    }

    @GetMapping("/candidates/{id}")
    public String findById(@PathVariable Long id, Model model){
        model.addAttribute("candidate", candidateService.findById(id));
        return "candidate";
    }

    @GetMapping("/create")
    public String addCandidate(Model model){
        model.addAttribute("candidateCommand", new CandidateCommand());
        return "createForm";
    }

    @PostMapping("/newCandidate")
        public String saveOrUpdate(@ModelAttribute @Valid CandidateCommand command){
        candidateService.saveCandidateCommand(command);
        System.out.println("saved or updated " + command.toString());
        return "redirect:/api/candidates/";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleNotFound(Exception e){

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("404");
        modelAndView.addObject("exception", e);

        return modelAndView;
    }
}
