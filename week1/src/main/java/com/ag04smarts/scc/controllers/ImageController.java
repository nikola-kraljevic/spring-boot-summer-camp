package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.services.CandidateService;
import com.ag04smarts.scc.services.ImageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/api")
public class ImageController {

    private final CandidateService candidateService;
    private final ImageService imageService;

    @Autowired
    public ImageController(CandidateService candidateService, ImageService imageService) {
        this.candidateService = candidateService;
        this.imageService = imageService;
    }

    @GetMapping("/candidates/{id}/img")
    public String imageForm(@PathVariable Long id, Model model){
        model.addAttribute("candidate", candidateService.findById(id));
        if (candidateService.findById(id).getImage() == null) {
            return "uploadimg";
        }
        return "image_exists";
    }

    @GetMapping("/candidates/{id}/img/show")
    public void showImage(@PathVariable Long id, HttpServletResponse response) throws IOException {

        CandidateCommand candidateCommand = candidateService.findCommandById(Long.valueOf(id));

        if (candidateCommand.getImage() != null){
            byte[] bytes = new byte[candidateCommand.getImage().length];
            int i = 0;

            for (byte wrappedByte : candidateCommand.getImage()){
                bytes[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(bytes);
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @PostMapping("/candidates/{id}/img")
    public String uploadImage(@PathVariable Long id, @RequestParam("profileImage") MultipartFile file){
        try {
            imageService.saveImageFile(id,file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/api/candidates/{id}/img/show";
    }
}
