package com.ag04smarts.scc.services;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.converters.CandidateCommandToCandidate;
import com.ag04smarts.scc.converters.CandidateToCandidateCommand;
import com.ag04smarts.scc.exceptions.NotFoundException;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;

@Service
public class CandidateServiceImpl implements CandidateService {

    private final CandidateRepository candidateRepository;
    private final CandidateCommandToCandidate candidateCommandToCandidate;
    private final CandidateToCandidateCommand candidateToCandidateCommand;

    @Autowired
    public CandidateServiceImpl(CandidateRepository candidateRepository, CandidateCommandToCandidate candidateCommandToCandidate, CandidateToCandidateCommand candidateToCandidateCommand) {
        this.candidateRepository = candidateRepository;
        this.candidateCommandToCandidate = candidateCommandToCandidate;
        this.candidateToCandidateCommand = candidateToCandidateCommand;
    }

    @Override
    public Iterable<Candidate> getCandidates(){
        return candidateRepository.findAll();
    }

    @Override
    public Candidate findById(Long l){
        Optional<Candidate> candidateOptional = candidateRepository.findById(l);
        if (!candidateOptional.isPresent()){
            throw new NotFoundException("User with id: " + l + " not found");
        }
        return candidateOptional.get();
    }

    @Override
    public CandidateCommand findCommandById(Long l){
        return candidateToCandidateCommand.convert(findById(l));
    }


    //the registration exists because of candidate constructor -- registration field is a new registration
    @Override
    @Transactional
    public CandidateCommand saveCandidateCommand(CandidateCommand command){

        Candidate detachedCandidate = candidateCommandToCandidate.convert(command);//convert obj from template to model

        Candidate savedCandidate = candidateRepository.save(detachedCandidate);//save model to db with middleman
        detachedCandidate.getRegistration().setCandidate(detachedCandidate);//connect candidate_id in registration with candidate
        return candidateToCandidateCommand.convert(savedCandidate);//return middleman
    }
}
