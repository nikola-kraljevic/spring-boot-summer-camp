package com.ag04smarts.scc.services;

import com.ag04smarts.scc.commands.RegistrationCommand;
import com.ag04smarts.scc.models.Registration;

public interface RegistrationService {

    Iterable<Registration> getRegistrations();

    Registration findById(Long l);


}
