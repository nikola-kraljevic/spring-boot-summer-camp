package com.ag04smarts.scc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class SCCApplication {

	public static void main(String[] args) {
		SpringApplication.run(SCCApplication.class, args);
	}

}
