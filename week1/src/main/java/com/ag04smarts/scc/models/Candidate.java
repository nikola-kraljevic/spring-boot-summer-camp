package com.ag04smarts.scc.models;

import com.ag04smarts.scc.validations.PhoneNumberConstraint;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Candidate extends Person{

    private String email;//todo unique fname lastnaem
    private Integer age;
    @PhoneNumberConstraint
    private String phoneNumber;
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @Lob
    private Byte[] image;
    @OneToOne(mappedBy = "candidate", cascade = CascadeType.ALL)
    @JsonIgnore
    private Registration registration = new Registration();
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Mentor mentor;

    public Candidate(){}

    public Candidate(String firstName, String lastName, String email, Integer age, String phoneNumber, Gender gender, Registration registration) {
        super(firstName, lastName);
        this.email = email;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.registration = registration;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public Byte[] getImage() {
        return image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public Mentor getMentor() {
        return mentor;
    }

    public void setMentor(Mentor mentor) {
        this.mentor = mentor;
    }

}
