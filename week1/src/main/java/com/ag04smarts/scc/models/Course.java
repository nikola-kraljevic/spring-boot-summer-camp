package com.ag04smarts.scc.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private CourseType type;
    @Min(value = 0, message = "nema mjesta na kursu xd")
    private Integer numberOfStudents;
    @ManyToMany(mappedBy = "courses")
    private List<Registration> registrations = new ArrayList<>();
    @ManyToMany
    @JoinTable
    private List<Lecturer> lecturers = new ArrayList<>();

    public Course(){}

    public Course(String name, CourseType type, Integer numberOfStudents) {
        this.name = name;
        this.type = type;
        this.numberOfStudents = numberOfStudents;
    }

    public Course(String name, CourseType type, Integer numberOfStudents, List<Registration> registrations, List<Lecturer> lecturers) {
        this.name = name;
        this.type = type;
        this.numberOfStudents = numberOfStudents;
        this.registrations = registrations;
        this.lecturers = lecturers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id =  id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CourseType getType() {
        return type;
    }

    public void setType(CourseType type) {
        this.type = type;
    }

    public Integer getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(Integer numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    public List<Lecturer> getLecturers() {
        return lecturers;
    }

    public void setLecturers(List<Lecturer> lecturers) {
        this.lecturers = lecturers;
    }
}
