package com.ag04smarts.scc.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date date;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "ID")
    @JsonIgnore
    private Candidate candidate;
    @ManyToMany
    @JoinTable
    @JsonIgnore
    private List<Course> courses = new ArrayList<>();

    public Registration(){
        this.date = new Date();
    }

    public Registration(Candidate candidate) {
        this.candidate = candidate;
    }

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "date=" + date +
                '}';
    }
}
