package com.ag04smarts.scc.models;

public enum CourseType {
    BASIC, ADVANCED, INTERMEDIATE
}
